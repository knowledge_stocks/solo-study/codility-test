// 예: 1041는 바이너리로 10000010001인데
// 여기서 binary gap은 1과 1사이의 0의 개수를 의미한다.
// 즉 1041은 바이너리 갭이 5와 3이다.
// 바이너리 갭의 최대값을 구하는 솔루션을 구현해라.

function getMaxBinaryGap(number) {
  const binaries = number.toString(2);

  let maxGap = 0;
  let gap = 0;
  for (const bin of [...binaries]) {
    if (bin === '1') {
      maxGap = Math.max(gap, maxGap);
      gap = 0;
    } else if (bin === '0') {
      gap++;
    }
  }

  return maxGap;
}

function solution(N) {
  return getMaxBinaryGap(N);
}
