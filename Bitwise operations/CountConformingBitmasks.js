// '11 1111 1111 1111 1111 1111 1011 1111'는 '11 1111 1111 1111 1111 1111 1001 1111'의 conform number이다.
// 0의 자리에 1이 더 들어가 있다면 confirm number라고 생각하면 된다. 즉 or 연산시 conform number와 동일하다.
// A,B,C의 conform number의 수를 구하는 솔루션을 구현해라.
//
// 구현은 어찌저찌 했는데, 정확도는 100점이지만 퍼포먼스 점수가 14점임

function solution(A, B, C) {
  const conformTestNums = new Set();
  conformTestNums.add(A);
  conformTestNums.add(B);
  conformTestNums.add(C);

  let idx = 0;
  while (idx < 30) {
    const tester = 1 << idx;
    conformTestNums.forEach((num) => {
      if ((tester & num) === 0) {
        conformTestNums.add(num | tester);
      }
    });
    idx++;
  }

  let conformCount = 0;
  conformTestNums.forEach((tester) => {
    if (
      (A | tester) === tester ||
      (B | tester) === tester ||
      (C | tester) === tester
    ) {
      conformCount++;
    }
  });

  return conformCount;
}

console.log(solution(1073741727, 1073741631, 1073741679));
