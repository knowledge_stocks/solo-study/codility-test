import cx from 'classnames';
import { Component } from 'react';

export default class LikeButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      likes: 100,
      isLiked: false,
    };
    this.toggleLike = this.toggleLike.bind(this);
  }

  toggleLike() {
    if (this.state.isLiked) {
      this.setState((state) => ({
        isLiked: false,
        likes: state.likes - 1,
      }));
    } else {
      this.setState((state) => ({
        isLiked: true,
        likes: state.likes + 1,
      }));
    }
  }

  render() {
    const { likes, isLiked } = this.state;
    return (
      <>
        <div>
          <button
            className={'like-button' + (isLiked ? ' liked' : '')}
            onClick={this.toggleLike}
          >
            {'Like | '}
            <span className='likes-counter'>{likes}</span>
          </button>
        </div>
        <style>{`
                    .like-button {
                        font-size: 1rem;
                        padding: 5px 10px;
                        color:  #585858;
                    }
                   .liked {
                        font-weight: bold;
                        color: #1565c0;
                   }
                `}</style>
      </>
    );
  }
}
